<?php
	session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Learn Php</title>
	<link rel="stylesheet" type="text/css" href="learnPHP.css">
</head>
<body>
	<button id='back'><a href='./index.html'>Go back</a></button>
	<h1>Learn PHP</h1>
	<h2>What is PHP?</h2>
	<ul>
		<li>PHP is an acronym for "PHP: Hypertext Preprocessor".</li>
		<li>PHP is a widely-used, open source scripting language.</li>
		<li>PHP scripts are executed on the server.</li>
		<li>PHP is free to download and use.</li>
	</ul>
	<h2>What can we do in PHP?</h2>

	
	<h3 class="lesson">&lt;?php ?&gt;</h3>
	<p class="meaning">This is the syntax we use to code in PHP.</p>
	
	<!-- echo -->
	<h3 class="lesson">echo</h3>
	<p class="meaning">The echo() function outputs one or more strings.</p>
	

	<!-- foreach -->
	<h3 class="lesson">foreach</h3>
	<p class="meaning">The foreach loop works only on arrays, and is used to loop through each key/value pair in an array.</p>
	<p>
		<?php
			$animals = array("cat", "dog", "panda", "bear");
			echo "<br>";
			print_r($animals);
			echo "<br>";
			echo "<br>";
			foreach ($animals as $value) {
			  echo "$value, ";
			}
		?>
	</p>

	<!-- for -->
	<h3 class="lesson">for</h3>
	<p class="meaning">The for loop is used when you know in advance how many times the script should run.</p>
	<p>
		<?php
			for($i=1; $i<=5; $i++){
				echo "$i, ";
			}
		?>
	</p>
	
	<!-- while -->
	<h3 class="lesson">while</h3>
	<p class="meaning">The while loop executes a block of code as long as the specified condition is true.</p>
	<p>
		<?php
			$i = 1;

			while($i <= 5) {
			  echo "$i, ";
			  $i++;
			}
		?>
	</p>
	
	<!-- if -->
	<h3 class="lesson">if</h3>
	<p class="meaning">The if statement executes some code if one condition is true.</p>
	<p>
		<?php
			$age = 20;
			if($age>17){
				echo "Are you legal age: TRUE";
			}
		?>
	</p>

	<!-- elseif -->
	<h3 class="lesson">elseif</h3>
	<p class="meaning">The elseif keyword tests a new condition if the condition of a previous if or elseif statement was not met. It is equivalent to putting an if statement inside an else block.</p>
	<p>
		<?php
			$age = 16;
			if($age>17){
				echo "Are you legal age: TRUE";
			}elseif($age<18){
				echo "Are you legal age: FALSE";
			}
		?>
	</p>
	
	<!-- else -->
	<h3 class="lesson">else</h3>
	<p class="meaning">The else keyword specifies a block of code which should run when the condition of an if statement is not met.</p>
	<p>
		<?php
			$age = 16;
			if($age>17){
				echo "Are you legal age: TRUE";
			}else{
				echo "Are you legal age: FALSE";
			}
		?>
	</p>

	<!-- switch -->
	<h3 class="lesson">switch</h3>
	<p class="meaning">The switch keyword is used to create a switch conditional. Switch conditionals choose a block of code to run based on the value of an expression.</p>
	<p>
		<?php
			$nationality = "filipino";
			switch ($nationality) {
				case "american":
				    echo "You're American!";
				    break;
				case "chinese":
				    echo "You're Chinese!";
				    break;
			    case "korean":
				    echo "You're Korean!";
				    break;
				default:
				    echo "You're Filipino!";
			}
		?>
	</p>

	<!-- require and require_once -->
	<h3 class="lesson">require and require_once</h3>
	<p class="meaning">The require keyword is used to embed PHP code from another file. If the file is not found, a fatal error is thrown and the program stops. The require_once keyword is used to embed PHP code from another file. If the file is not found, a fatal error is thrown and the program stops. If the file was already included previously, this statement will not include it again.</p>

	<!-- isset -->
	<h3 class="lesson">isset</h3>
	<p class="meaning">The isset() function checks whether a variable is set, which means that it has to be declared and is not NULL. This function returns true if the variable exists and is not NULL, otherwise it returns false.</p>
	<p>
		<?php
			$x = 5;
			if(isset($x)){
				echo "X is 'set'.";
			}else{
				echo "X is not 'set'.";
			}
		?>
	</p>

	<!-- sha1 and md5 -->
	<h3 class="lesson">sha1 and md5</h3>
	<p class="meaning">The sha1() function calculates the SHA-1 hash of a string. The md5() function calculates the MD5 hash of a string.</p>
	<p>
		<?php
			$msg = "programmer";
			echo "This is the message in sha1: ". sha1($msg)."<br> <br>";
			echo "This is the message in md5: ". md5($msg);		
		?>
	</p>

	<!-- round -->
	<h3 class="lesson">round</h3>
	<p class="meaning">The round() function rounds a floating-point number.</p>
	<p>
		<?php
			$num = 1985.76;
			echo "The rounded number for $num is: ". round($num);	
		?>
	</p>

	<!-- ob_start -->
	<h3 class="lesson">ob_start</h3>
	<p class="meaning"> The ob_start() function creates an output buffer. A callback function can be passed in to do processing on the contents of the buffer before it gets flushed from the buffer. Flags can be used to permit or restrict what the buffer is able to do.
	</p>
	<p>
		<?php
			ob_start();
			echo "This message will not be sent to the browser.";
			ob_end_clean();

			echo "This message will be sent to the browser.";	
		?>
	</p>

	<!-- ob_end_flush -->
	<h3 class="lesson">ob_end_flush</h3>
	<p class="meaning"> The ob_end_flush() function deletes the topmost output buffer and outputs all of its contents. The output may be caught by another output buffer, or, if there are no other output buffers, sent directly to the browser.</p>
	<p>
		<?php
			ob_start();
			echo "This message sent to the browser.";
			ob_end_flush();	
		?>
	</p>

	<!-- array -->
	<h3 class="lesson">array</h3>
	<p class="meaning"> An array is a special variable, which can hold more than one value at a time.</p>
	<p>
		<?php
			$onlineGames = array("LoL", "Dota", "Valorant");
			echo "<br>";
			print_r($onlineGames);
		?>
	</p>

	<!-- count -->
	<h3 class="lesson">count</h3>
	<p class="meaning"> The count() function is used to return the length (the number of elements) of an array.</p>
	<p>
		<?php
			$onlineGames = array("LoL", "Dota", "Valorant");
			echo "<br>";
			print_r($onlineGames);
			echo "<br> <br>";
			echo "The length of the array is ". count($onlineGames);
		?>
	</p>

	<!-- ini_set -->
	<h3 class="lesson">ini_set</h3>
	<p class="meaning"> Sets the value of a configuration option.</p>
	<p>
		<?php
			echo ini_get('display_errors');

			if (!ini_get('display_errors')) {
			    ini_set('display_errors', '1');
			}

			echo ini_get('display_errors');
		?>
	</p>

	<!-- htmlentities -->
	<h3 class="lesson">htmlentities</h3>
	<p class="meaning"> The htmlentities() function converts characters to HTML entities.</p>
	<p>
		<?php
			$str = "<a href='https://www.google.com/'>Google</a>";
			echo htmlentities($str);
		?>
	</p>

	<!-- addslashes -->
	<h3 class="lesson">addslashes</h3>
	<p class="meaning"> Add a backslash in front of each double quote ("):</p>
	<p>
		<?php
			$msg = 'Hello there, "Summoner"!.';
			echo addslashes($msg);
		?>
	</p>

	<!-- stripslashes -->
	<h3 class="lesson">stripslashes</h3>
	<p class="meaning"> The stripslashes() function removes backslashes added by the addslashes() function.</p>
	<p>
		<?php
			$msg = addslashes($msg);
			echo $msg. "<br> <br>";
			echo stripslashes($msg);
		?>
	</p>

	<!-- Associative array -->
	<h3 class="lesson">Associative array</h3>
	<p class="meaning"> Associative arrays are arrays that use named keys that you assign to them.</p>
	<p>
		<?php
			$language = array("America"=>"English", "Philippines"=>"Filipino/Tagalog", "China"=>"Chinese");

			foreach($language as $nation => $nationLang) {
			  echo "The language in " . $nation . " is " . $nationLang . ", ";
			}
		?>
	</p>

	<!-- Multi-dimensional array -->
	<h3 class="lesson">Multi-dimensional array</h3>
	<p class="meaning"> A multidimensional array is an array containing one or more arrays. PHP supports multi-dimensional arrays that are two, three, four, five, or more levels deep. However, arrays more than three levels deep are hard to manage for most people.</p>
	<p>
		<?php
			$people = array (
			  array("Joshua ", 22, " Male"),
			  array("Eya ", 35, " Female"),
			  array("Saab ", 5, " Male"),
			  array("Ethan ", 17, " Female")
			);

			for($i=0; $i<count($people); $i++){
				for($x=0; $x<count($people[$i]); $x++){
					echo $people[$i][$x]." ";
				}
				echo " | ";
			}
		?>
	</p>

	<!-- explode -->
	<h3 class="lesson">explode</h3>
	<p class="meaning">The explode() function breaks a string into an array.</p>
	<p>
		<?php
			$str = "Hello there! I hope you have a wonderful day!";
			echo $str."<br> <br>";
			print_r(explode(" ", $str));
		?>
	</p>

	<!-- implode -->
	<h3 class="lesson">implode</h3>
	<p class="meaning">The implode() function returns a string from the elements of an array.</p>
	<p>
		<?php
			$arr = array("Hello there!", " I hope you", "have", "a wonderful day!");
			echo "<br>";
			print_r($arr);
			echo "<br> <br>";
			echo implode(" ", $arr);
		?>
	</p>

	<!-- str_replace -->
	<h3 class="lesson">str_replace</h3>
	<p class="meaning">The str_replace() function replaces some characters with some other characters in a string.</p>
	<p>
		<?php
			$fruits = array("apple", "banana", "orange", "watermelon");
			echo "<br>";
			print_r($fruits);
			echo "<br> <br>";
			print_r(str_replace("orange","kiwi", $fruits));
		?>
	</p>

	<!-- strlen -->
	<h3 class="lesson">strlen</h3>
	<p class="meaning">The strlen() function returns the length of a string.</p>
	<p>
		<?php
			echo $str."<br> <br>";
			echo "The length of the string is ". strlen($str);
		?>
	</p>

	<!-- substr -->
	<h3 class="lesson">substr</h3>
	<p class="meaning">The substr() function returns a part of a string.</p>
	<p>
		<?php
			echo $str."<br> <br>";
			echo "After the substr: ". substr($str, 5);
		?>
	</p>

	<!-- trim -->
	<h3 class="lesson">trim</h3>
	<p class="meaning">The trim() function removes whitespace and other predefined characters from both sides of a string.</p>
	<p>
		<?php
			$str = "Programming is fun!";
			echo $str."<br> <br>";
			echo "After the trim: ". trim($str, "Pro");
		?>
	</p>

	<!-- strrev -->
	<h3 class="lesson">strrev</h3>
	<p class="meaning">The strrev() function reverses a string.</p>
	<p>
		<?php
			$str = "Programming is fun!";
			echo $str."<br> <br>";
			echo "After the strrev: ". strrev($str);
		?>
	</p>

	<!-- strpos -->
	<h3 class="lesson">strpos</h3>
	<p class="meaning">The strpos() function finds the position of the first occurrence of a string inside another string.</p>
	<p>
		<?php
			$str = "Programming is fun!";
			echo $str."<br> <br>";
			echo "Find the position of 'is': ". strpos($str, "is");
		?>
	</p>

	<!-- min -->
	<h3 class="lesson">min</h3>
	<p class="meaning">The min() function returns the lowest value in an array, or the lowest value of several specified values.</p>
	<p>
		<?php
			$arrNum = array(2, 6, 9, 3, 10, 4);
			echo "<br>";
			print_r($arrNum);
			echo "<br> <br>";
			echo "The lowest value of is: ". min($arrNum);
		?>
	</p>

	<!-- max -->
	<h3 class="lesson">max</h3>
	<p class="meaning">The max() function returns the highest value in an array, or the highest value of several specified values.</p>
	<p>
		<?php
			$arrNum = array(2, 6, 9, 3, 10, 4);
			echo "<br>";
			print_r($arrNum);
			echo "<br> <br>";
			echo "The highest value of is: ". max($arrNum);
		?>
	</p>

	<!-- abs -->
	<h3 class="lesson">abs</h3>
	<p class="meaning">The abs() function returns the absolute (positive) value of a number.</p>
	<p>
		<?php
			$arrNum = array(-2, -6, -9, -3, -10, -4);
			echo "<br>";
			print_r($arrNum);
			echo "<br> <br>";
			echo "The absolute values of the numbers in array are: ";
			foreach($arrNum as $absNum){
				echo abs($absNum).", ";
			}
		?>
	</p>

	<!-- strtoupper -->
	<h3 class="lesson">strtoupper</h3>
	<p class="meaning">The strtoupper() function converts a string to uppercase.</p>
	<p>
		<?php
			echo $str."<br> <br>";
			echo "After the strtoupper function: ". strtoupper($str);
		?>
	</p>

	<!-- strtolower -->
	<h3 class="lesson">strtolower</h3>
	<p class="meaning">The strtolower() function converts a string to lowercase.</p>
	<p>
		<?php
		    $str = "HELLO WORLD!";
			echo $str."<br> <br>";
			echo "After the strtolower function: ". strtolower($str);
		?>
	</p>

	<!-- $GLOBALS -->
	<h3 class="lesson">$GLOBALS</h3>
	<p class="meaning">$GLOBALS is a PHP super global variable which is used to access global variables from anywhere in the PHP script (also from within functions or methods). PHP stores all global variables in an array called $GLOBALS[index]. The index holds the name of the variable.</p>

	<!-- $_SERVER -->
	<h3 class="lesson">$_SERVER</h3>
	<p class="meaning">$_SERVER is a PHP super global variable which holds information about headers, paths, and script locations.</p>
	<p>
		<?php
			echo "The Server: " .$_SERVER['PHP_SELF'];
			echo "<br>";
			echo "The Server name: " .$_SERVER['SERVER_NAME'];
			echo "<br>";
			echo "The Server host: " .$_SERVER['HTTP_HOST'];
			echo "<br>";
			echo "The Server referer: " .$_SERVER['HTTP_REFERER'];
			echo "<br>";
			echo "The HTTP user agent: " .$_SERVER['HTTP_USER_AGENT'];
			echo "<br>";
			echo "The Script name: " .$_SERVER['SCRIPT_NAME'];
		?>
	</p>

	<!-- $_POST -->
	<h3 class="lesson">$_POST</h3>
	<p class="meaning">PHP $_POST is a PHP super global variable which is used to collect form data after submitting an HTML form with method="post". $_POST is also widely used to pass variables.</p>

	<form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>">
	  	<label>Name:</label>
	  	<input type="text" name="fname">
	  	<input type="submit">
	</form>
	<p>
		<?php
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				  $name = $_POST['fname'];
				  if (empty($name)) {
				    echo "EMPTY!";
				  } else {
				    echo $name;
				  }
			}
		?>
	</p>

	<!-- $_GET -->
	<h3 class="lesson">$_GET</h3>
	<p class="meaning">PHP $_GET is a PHP super global variable which is used to collect form data after submitting an HTML form with method="get". $_GET can also collect data sent in the URL.</p>

	<form method="GET">
	  	<label>Name:</label>
	  	<input type="text" name="fname">
	  	<input type="submit">
	</form>
	<p>Look at the URL</p>

	<!-- $_COOKIE -->
	<h3 class="lesson">$_COOKIE</h3>
	<p class="meaning">A cookie is often used to identify a user. A cookie is a small file that the server embeds on the user's computer. Each time the same computer requests a page with a browser, it will send the cookie too. With PHP, you can both create and retrieve cookie values.</p>

	<!-- $_SESSION -->
	<h3 class="lesson">$_SESSION</h3>
	<p class="meaning">A session is a way to store information (in variables) to be used across multiple pages. Unlike a cookie, the information is not stored on the users computer.</p>
	<p>
		<?php
			$_SESSION['name'] = "Db";
			echo "The session name is: ". $_SESSION['name']."<br> <br>";
			if(isset($_SESSION['name'])){
				echo "The session is set!";
			}else{
				echo "The session is not set!";
			}
		?>
	</p>
</body>
</html>